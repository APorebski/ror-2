class PlanetarySystems::NewPresenter < Curly::Presenter

  def newSystem
    "New planetary system"
  end

  def backLink
    link_to 'Back', planetary_systems_path
  end

  def system_form(&block)
    form_for(PlanetarySystem.new, &block)
  end
end
