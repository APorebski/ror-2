class PlanetarySystems::PlanetarySystemPresenter < Curly::Presenter
  presents :planetarySystem

  def name
    @planetarySystem.name
  end

  def image
    image_tag(@planetarySystem.image_url(:thumbnail))
  end

  def age
    @planetarySystem.age
  end

  def years
    if @planetarySystem.age > 1
      "mld years"
    else
      "mld years"
    end
  end

  def link
    link_to 'Show', planetary_system_path(@planetarySystem)
  end
end
