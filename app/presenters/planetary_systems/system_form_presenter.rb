class PlanetarySystems::SystemFormPresenter < Curly::Presenter
  presents :system_form

  def nameFieldLabel
    @system_form.label :name, class: 'col-2 col-form-label'
  end

  def nameField
    @system_form.text_field :name, class: 'form-control'
  end

  def ageFieldLabel
    @system_form.label :age, class: 'col-2 col-form-label'
  end

  def ageField
    @system_form.number_field :age, class: 'form-control', min: 0
  end

  def descriptionFieldLabel
    @system_form.label :description, class: 'col-2 col-form-label'
  end

  def mdSimpleEditor
    md_simple_editor do
      @system_form.text_area :description
    end
  end

  def imageFieldLabel
    @system_form.label :image, class: 'col-2 col-form-label'
  end

  def imageField
    @system_form.file_field :image, onchange: "validateFiles(this);", data: { max_file_size: 5.megabytes }, class: 'form-control', dragdrop: true
  end

  def submit
    @system_form.submit class: 'btn btn-primary'
  end
end
