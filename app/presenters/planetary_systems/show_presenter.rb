class PlanetarySystems::ShowPresenter < Curly::Presenter
  presents :planetarySystem

  def flash?
    flash[:notice]
  end

  def flashInfo
    flash[:notice]
  end

  def name
    @planetarySystem.name
  end

  def email
    @planetarySystem.user.email
  end

  def time
    time_ago_in_words(@planetarySystem.created_at)
  end

  def age
    @planetarySystem.age
  end

  def years
    if @planetarySystem.age > 1
      "mld years"
    else
      "mld years"
    end
  end

  def signedIn?
    user_signed_in?
  end

  def idMatch?
    @planetarySystem.user.id == current_user.id
  end

  def editLink
    link_to 'Edit', edit_planetary_system_path(@planetarySystem), class: "btn btn-primary"
  end

  def deleteLink
    link_to 'Destroy', planetary_system_path(@planetarySystem),
    method: :delete,
    data: { confirm: 'Are you sure?' }, class: "btn btn-primary"
  end

  def backLink
    link_to 'Back', planetary_systems_path, class: "btn btn-primary"
  end

  def description
    @planetarySystem.description
  end

  def image
    image_tag(@planetarySystem.image_url(:standard))
  end
end
