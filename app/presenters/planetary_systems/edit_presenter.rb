class PlanetarySystems::EditPresenter < Curly::Presenter

  def editSystem
    "Editing planetary system"
  end

  def backLink
    link_to 'Back', planetary_systems_path
  end

  def system_form(&block)
    form_for(PlanetarySystem.new, &block)
  end
end
