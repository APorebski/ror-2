class PlanetarySystems::IndexPresenter < Curly::Presenter
  presents :planetarySystems

  def title
    "Planetary systems"
  end

  def flash?
    flash[:notice]
  end

  def flashInfo
    flash[:notice]
  end

  def planetarySystems
    @planetarySystems
  end

end
