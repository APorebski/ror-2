class PlanetarySystem < ApplicationRecord
  belongs_to :user
  mount_uploader :image, PictureUploader
  validates :name, presence: true, length: { minimum: 3, maxium: 30}
  validates :description, length: {maximum: 1500}
  validates :age, numericality: { only_integer: true , greater_than_or_equal_to: 1 }
end
