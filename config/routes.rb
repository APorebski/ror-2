Rails.application.routes.draw do
  devise_for :users
  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  resources :planetary_systems

  delete '/removeImage/:id', to: 'planetary_systems#removeImage',
          as: 'removeImage'

  get "*path", to: redirect('/')
end
