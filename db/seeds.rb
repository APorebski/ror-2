# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
user = User.new
user.email = 'admin@admin.pl'
user.password = "adminadmin"
user.password_confirmation = "adminadmin"
user.save!

seed_PS = [
  ["Sun", 5, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491771953/Sun_white_itwn26.jpg",
  "The Sun is the star at the center of the Solar System. It is a nearly perfect sphere of hot plasma,
  with internal convective motion that generates a magnetic field via a dynamo process. It is by far
  the most important source of energy for life on Earth. Its diameter is about 109 times that of Earth,
   and its mass is about 330,000 times that of Earth, accounting for about 99.86% of the total mass of
   the Solar System.[16] About three quarters of the Sun's mass consists of hydrogen (~73%); the rest is
   mostly helium (~25%), with much smaller quantities of heavier elements, including oxygen, carbon, neon,
   and iron.

  The Sun is a G-type main-sequence star (G2V) based on its spectral class, and is informally referred to
  as a yellow dwarf. It formed approximately 4.6 billion years ago from the gravitational collapse
  of matter within a region of a large molecular cloud. Most of this matter gathered in the center, whereas
  the rest flattened into an orbiting disk that became the Solar System. The central mass became so hot
  and dense that it eventually initiated nuclear fusion in its core. It is thought that almost all stars
   form by this process."],

   ["82 G. Eridani", 6, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491771953/320px-Eridanus_constellation_map.svg_knckad.png",
   "On August 17, 2011, European astronomers announced the discovery of three planets orbiting 82 G. Eridani.
   The mass range of these planets classifies them as super-Earths; objects with only a few times the Earth's
   mass. These planets were discovered by precise measurements of the radial velocity of the star, with the
   planets revealing their presence by their gravitational displacement of the star during each orbit. None
   of the planets display a significant orbital eccentricity. However, their orbital periods are all 90 days
   or less, indicating that they are orbiting close to the host star. The equilibrium temperature for the most
   distant planet, based on an assumed Bond albedo of 0.3, would be about 388 K (115 °C); significantly
   above the boiling point of water.

  The number of planets in the system is slightly uncertain. At the time of planet c's detection, it exerted
  the lowest gravitational perturbation. There was also a similarity noted between its orbital period and the
   rotational period of the star. For these reasons the discovery team were somewhat more cautious regarding
   the verity of its candidate planet status than for the other two. Continued observation of the star will
   be required to determine the exact nature of the planetary system."],

    ["Gliese 581", 9, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491771953/SunGliese581_mgt21l.png",
    "Observations suggest that the star has a planetary system consisting of three confirmed planets,
    designated Gliese 581b, c, and e and a possible fourth, d, lettered in order of discovery. Additional
    outer planets, which received the designations Gliese 581 f and g, have been proposed, but the
    observations that led to the discovery claims has been shown to be the result of stellar activity
    mimicking radial velocity variations due to orbiting planets."],

    ["Gliese 667", 11, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491771953/320px-Scorpius_constellation_map.svg_cgx9ee.png",
    "The two brightest stars in this system, GJ 667 A and GJ 667 B, are orbiting each other at an average
    angular separation of 1.81 arcseconds with a high eccentricity of 0.58. At the estimated distance of
    this system, this is equivalent to a physical separation of about 12.6 AU, or nearly 13 times the
    separation of the Earth from the Sun. Their eccentric orbit brings the pair as close as about 5 AU
    to each other, or as distant as 20 AU, corresponding to an eccentricity of 0.6. This
    orbit takes approximately 42.15 years to complete and the orbital plane is inclined at an angle of
    128° to the line of sight from the Earth. The third star, GJ 667 C, orbits the GJ 667 AB pair at
    an angular separation of about 30, which equates to a minimum separation of 230 AU."],

    ["HR 8832", 9, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491771954/PIA19832-StarHD219134-Location-20150730_yxf2tc.jpg",
    "HR 8832 (or HD 219134, or Gliese 892) is a main sequence star in the constellation of Cassiopeia.
    It is smaller and less luminous than our Sun, with a spectral class of K3V, which makes it an
    orange-red hued star. HR 8832 is relatively close to our system, with an estimated distance of
    21.25 light years. This star is close to the limit of apparent magnitude that can still be seen
    by the unaided eye. The limit is considered to be magnitude 6 for most observers.

  This star has a magnitude 9.4 companion at an angular separation of 106.6 arcseconds The star is
   reported to host a rocky super-Earth, HD 219134 b, based on size (1.6 times the size of Earth),
   and density (6 grams per cubic cm). A further three exoplanets, two super-Earths and one
   Jovian world, have been deduced using Harps-N radial velocity data. Two more were discovered two months later."],

    ["61 Virginis", 12, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491771953/1024px-61_Vir_as_seen_with_a_12.5-_telescope_with_a_field_of_view_of_45.1_arcminutes_uaisss.jpg",
    "61 Virginis is a fifth-magnitude G-type main-sequence star with a stellar classification of G7 V. It is faint
    but visible to the naked eye south and east of the bright star Spica in the zodiac constellation of Virgo. The
    designation 61 Virginis originated in the star catalogue of English astronomer John Flamsteed, as part of his
    Historia Coelestis Britannica. An 1835 account of Flamsteed's work by English astronomer Francis Baily noted
    that the star showed a proper motion. This made the star of interest for parallax studies, and by 1950 a
    mean annual value of 0.006″ was obtained. The present day result, obtained with data from the Hipparcos satellite,
    gives a parallax of 116.89 mas, which corresponds to a physical separation of 27.9 light years from the Sun."],

    ["54 Piscium", 5, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491771954/LuhmanTstarSpitzer__54_Psc_siwsiv.jpg",
   "The Flamsteed designation 54 Piscium originated in the star catalogue of the British astronomer John Flamsteed,
   first published in 1712. It has an apparent magnitude of 5.86, allowing it to be seen with the unaided eye under
   suitable viewing conditions.The star has a classification of K0V, with the luminosity class V indicating this is
   a main sequence star that is generating energy at its core through the thermonuclear fusion of hydrogen into helium.
   The effective temperature of the photosphere is about 5,062 K, giving it the characteristic orange hue of a K-type star."],

    ["TRAPPIST-1", 3, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491772347/PIA21429_-_Transit_Illustration_of_TRAPPIST-1__cropped_ez5tzf.jpg",
  	"The system is very flat and compact. All seven of TRAPPIST-1's planets orbit much closer than Mercury orbits the Sun.
  	Except for TRAPPIST-1b, they orbit farther than the Galilean satellites do around Jupiter, but closer than most of the
  	other moons of Jupiter. The distance between the orbits of TRAPPIST-1b and TRAPPIST-1c is only 1.6 times the distance
  	between the Earth and the Moon. The planets should appear prominently in each other's skies, in some cases appearing
  	several times larger than the Moon appears from Earth. A year on the closest planet passes in only 1.5 Earth days,
  	while the seventh planet's year passes in only 18.8 days."],

    ["55 Cancri", 5, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491772347/606px-Cancer_IAU.svg_qhxzvp.png",
  	"The 55 Cancri system was the first known to have four, and later five planets, and may possibly have more. The innermost
  	planet, e, transits 55 Cancri A as viewed from Earth. The next planet, b, is non-transiting but there is tentative evidence
  	that it is surrounded by an extended atmosphere that does transit the star.

  In 1997, the discovery of a 51 Pegasi-like planet orbiting 55 Cancri A was announced, together with the planet of Tau Boötis
  and the inner planet of Upsilon Andromedae. The planet was discovered by measuring the star's radial velocity, which showed a
   periodicity of around 14.7 days corresponding to a planet at least 78% of the mass of Jupiter. These radial velocity measurements
   still showed a drift unaccounted-for by this planet, which could be explained by the gravitational influence of a more distant object."],

    ["HD 69830", 5, 1, "http://res.cloudinary.com/di3jhhujc/image/upload/v1491772349/Morgan-Keenan_spectral_classification_zoom_iwmxnd.png",
  	"On May 17, 2006, a team of astronomers using the European Southern Observatory's (ESO) HARPS spectrograph on the 3.6-metre La Silla
  	telescope in the Atacama desert, Chile, announced the discovery of three extrasolar planets orbiting the star. With minimum masses
  	between 10 and 18 times that of the Earth, all three planets are presumed to be similar to the planets Neptune or Uranus. As of 2011,
  	no planet with more than half the mass of Jupiter had been detected within three astronomical units of HD 69830."],
]

seed_PS.each do |name, age, user_id, image, description|
  PlanetarySystem.create(name: name, age: age, description: description, remote_image_url: image, user_id: user_id)
end

#10.times do
#  PlanetarySystem.create([{
#                      name: Faker::Space.unique.galaxy,
#                      age: Faker::Number.between(1, 1000),
#                      description: Faker::Hacker.say_something_smart,
#                      user_id: 1
#                  }])
#end
