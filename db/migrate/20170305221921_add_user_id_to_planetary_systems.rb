class AddUserIdToPlanetarySystems < ActiveRecord::Migration[5.0]
  def change
    add_column :planetary_systems, :user_id, :integer
  end
end
