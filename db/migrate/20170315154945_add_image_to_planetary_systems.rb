class AddImageToPlanetarySystems < ActiveRecord::Migration[5.0]
  def change
    add_column :planetary_systems, :image, :string
  end
end
